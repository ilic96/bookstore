﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBookstore.Models;
using EBookstore.Models.Filter;
using Microsoft.Ajax.Utilities;
using PagedList;

namespace EBookstore.Controllers
{
    public class BooksController : Controller
    {
        private EBookstoreContext db = new EBookstoreContext();

        public enum Sorting
        {
            Name,
            Author,
            Price,
            Date
        }

        private static Dictionary<string, Sorting> sortDic = new Dictionary<string, Sorting>()
        {
            {"Name", Sorting.Name },
            {"Author", Sorting.Author },
            {"Price", Sorting.Price },
            {"Date", Sorting.Date }
        };

        SelectList list = new SelectList(sortDic, "Key", "Key", "Name");

        private int pageSize = 5;

        // GET: Books
        public ActionResult Index(BookFilter filter, string sortType = "Name", int pageNum=1)
        {
            IEnumerable<Book> allBooks = db.Books.Include(b => b.Bookstore);

            Sorting sortBy = sortDic[sortType];

            switch (sortBy)
            {
                case Sorting.Name:
                    allBooks = allBooks.OrderBy(b => b.Name);
                    break;
                case Sorting.Author:
                    allBooks = allBooks.OrderBy(b => b.Author);
                    break;
                case Sorting.Price:
                    allBooks = allBooks.OrderBy(b => b.Price);
                    break;
                case Sorting.Date:
                    allBooks = allBooks.OrderBy(b => b.Created);
                    break;
            }

            if(sortType == "")
            {
                sortType = "Name";
            }
            if (!filter.Name.IsNullOrWhiteSpace())
            {
                allBooks = allBooks.Where(f => f.Name.Contains(filter.Name));
            }
            if (!filter.Author.IsNullOrWhiteSpace())
            {
                allBooks = allBooks.Where(f => f.Author.Contains(filter.Author));
            }
            if(filter.Price != 0.00)
            {
                allBooks = allBooks.Where(f => f.Price == filter.Price);
            }
            ViewBag.CurrentFilter = filter;
            ViewBag.CurrentSortType = sortType;
            ViewBag.Sorting = list;
            return View(allBooks.ToPagedList(pageNum, pageSize));

        }

        // GET: Books/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            ViewBag.BookstoreId = new SelectList(db.Bookstores, "Id", "Name");
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Author,genre,Price,Created,BookstoreId")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Books.Add(book);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BookstoreId = new SelectList(db.Bookstores, "Id", "Name", book.BookstoreId);
            return View(book);
        }

        // GET: Books/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookstoreId = new SelectList(db.Bookstores, "Id", "Name", book.BookstoreId);
            return View(book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Author,genre,Price,Created,BookstoreId")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookstoreId = new SelectList(db.Bookstores, "Id", "Name", book.BookstoreId);
            return View(book);
        }

        // GET: Books/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
