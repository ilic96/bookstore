﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EBookstore.Models;

namespace EBookstore.Controllers
{
    public class BookstoresController : Controller
    {
        private EBookstoreContext db = new EBookstoreContext();

        // GET: Bookstores
        public ActionResult Index()
        {
            return View(db.Bookstores.ToList());
        }

        // GET: Bookstores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bookstore bookstore = db.Bookstores.Find(id);
            if (bookstore == null)
            {
                return HttpNotFound();
            }
            return View(bookstore);
        }

        // GET: Bookstores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Bookstores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Street,City")] Bookstore bookstore)
        {
            if (ModelState.IsValid)
            {
                db.Bookstores.Add(bookstore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bookstore);
        }

        // GET: Bookstores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bookstore bookstore = db.Bookstores.Find(id);
            if (bookstore == null)
            {
                return HttpNotFound();
            }
            return View(bookstore);
        }

        // POST: Bookstores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Street,City")] Bookstore bookstore)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bookstore).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bookstore);
        }

        // GET: Bookstores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bookstore bookstore = db.Bookstores.Find(id);
            if (bookstore == null)
            {
                return HttpNotFound();
            }
            return View(bookstore);
        }

        // POST: Bookstores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bookstore bookstore = db.Bookstores.Find(id);
            db.Bookstores.Remove(bookstore);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
