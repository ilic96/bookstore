namespace EBookstore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelsandControllers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Author = c.String(nullable: false, maxLength: 50),
                        genre = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        Created = c.DateTime(nullable: false),
                        BookstoreId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bookstores", t => t.BookstoreId, cascadeDelete: true)
                .Index(t => t.BookstoreId);
            
            CreateTable(
                "dbo.Bookstores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Street = c.String(nullable: false, maxLength: 50),
                        City = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "BookstoreId", "dbo.Bookstores");
            DropIndex("dbo.Books", new[] { "BookstoreId" });
            DropTable("dbo.Bookstores");
            DropTable("dbo.Books");
        }
    }
}
