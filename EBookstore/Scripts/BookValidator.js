﻿function validation() {

    var nameElement = $("#name").val();
    var authorElement = $("#author").val();
    var priceElement = $("#price").val();
  //  var dateElement = $("#date").val();


    var flagName = true;
    var flagAuthor = true;
    var flagPrice = true;
    //var flagDate = isFutureDate(dateElement);

    if (nameElement.length < 3) {
        flagName = false;
    }
    if (authorElement.length < 5) {
        flagAuthor = false;
    }
    if (priceElement <= 200.00) {
        flagPrice = false;
    }

    if (flagName) {
        hideDiv("#nameError");
    }
    else {
        showDiv("#nameError");
    }
    if (flagAuthor) {
        hideDiv("#authorError");
    }
    else {
        showDiv("#authorError");
    }
    if (flagName) {
        hideDiv("#priceError");
    }
    else {
        showDiv("#priceError");
    }
 /*   if (flagDate) {
        hideDiv("#createdError");
    }
    else {
        showDiv("#createdError");
    }*/
    if (flagName && flagAuthor && flagPrice) {
        $("#formCreate").submit();
    }
}

function hideDiv(idDiv) {
    $(idDiv).removeClass("text-danger");
    $(idDiv).addClass("hidden");
}

function showDiv(idDiv) {
    $(idDiv).removeClass("hidden");
    $(idDiv).addClass("text-danger");
}
/*
function isFuturedDate(inputDate) {
    var myDate = new Date(inputDate);
    var today = new Date();
    if (myDate > today) {
        return false;
    }
    return true;
}*/

$("#submitButton").click(validation);