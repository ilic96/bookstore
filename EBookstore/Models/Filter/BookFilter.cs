﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EBookstore.Models.Filter
{
    public class BookFilter
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public double Price { get; set; }

    }
}