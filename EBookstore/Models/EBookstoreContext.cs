﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EBookstore.Models
{
    public class EBookstoreContext:DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Bookstore> Bookstores { get; set; }

        public EBookstoreContext():base("name=BookstoreContext")
        {

        }
    }
}