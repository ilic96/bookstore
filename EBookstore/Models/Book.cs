﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EBookstore.Models
{
    public enum Genre
    {
        Science,
        Horror,
        Comedy,
        Crime,
        History
    }
    public class Book
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Author { get; set; }
        [Display(Name = "Genre")]
        public Genre genre { get; set; }
        [Required]
        [Range(0, double.MaxValue)]
        [Display(Name = "RSD")]
        public double Price { get; set; }
        [Required]
        public DateTime Created { get; set; }
        [ForeignKey("Bookstore")]
        public int BookstoreId { get; set; }
        public Bookstore Bookstore { get; set; }
    }
}